CentOS 7 - Package Repository
=============================

Repository Setup
----------------

Install with Yum repository.

```
% sudo tee -a /etc/yum.repos.d/kjdev.repo <<EOM
[kjdev]
name=CentOS \$releasever - kjdev
baseurl=https://gitlab.com/kjdev/el\$releasever.pkg/raw/master/rpm/
enabled=0
gpgcheck=0

[kjdev-source]
name=CentOS \$releasever - kjdev - Source
baseurl=https://gitlab.com/kjdev/el\$releasever.pkg/raw/master/source/
enabled=0
gpgcheck=0
EOM
```

RPM Install
-----------

```
% yum --enablerepo=kjdev install <PACKAGE>
```
